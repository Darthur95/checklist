import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Check } from '../check';

@Component(
  {
    selector: 'app-check-list',
    templateUrl: './check-list.component.html',
    styleUrls: ['./check-list.component.css']
  }
)
export class CheckListComponent {

  @Input()
  checks: Check[];

  @Output()
  remove: EventEmitter<Check> = new EventEmitter();

  @Output()
  toggleComplete: EventEmitter<Check> = new EventEmitter();

  constructor() {
  }

  onToggleCheckComplete(check: Check) {
    this.toggleComplete.emit(check);
  }

  onRemoveCheck(check: Check) {
    this.remove.emit(check);
  }

}
