import { Component, Input } from '@angular/core';
import { Check } from '../check';

@Component({
  selector: 'app-check-list-footer',
  templateUrl: './check-list-footer.component.html',
  styleUrls: ['./check-list-footer.component.css']
})
export class CheckListFooterComponent {

  @Input()
  checks: Check[];

  constructor() {
  }

}
