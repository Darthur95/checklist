/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CheckListFooterComponent } from './check-list-footer.component';
import { Check } from '../check';

describe('CheckListFooterComponent', () => {
  let component: CheckListFooterComponent;
  let fixture: ComponentFixture<CheckListFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckListFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckListFooterComponent);
    component = fixture.componentInstance;
    component.checks = [
      new Check({ id: 1, title: 'Test', complete: false })
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
