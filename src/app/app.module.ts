import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CheckListComponent } from './check-list/check-list.component';
import { CheckListFooterComponent } from './check-list-footer/check-list-footer.component';
import { CheckListHeaderComponent } from './check-list-header/check-list-header.component';
import { CheckDataService } from './check-data.service';
import { CheckListItemComponent } from './check-list-item/check-list-item.component';
import { ApiService } from './api.service';
import { SignInComponent } from './sign-in/sign-in.component';
import { ChecksComponent } from './checks/checks.component';
import { AuthService } from './auth.service';
import { SessionService } from './session.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CheckListComponent,
    CheckListFooterComponent,
    CheckListHeaderComponent,
    CheckListItemComponent,
    SignInComponent,
    ChecksComponent,
    CheckListItemComponent,
    PageNotFoundComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    AuthService,
    SessionService,
    CheckDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
