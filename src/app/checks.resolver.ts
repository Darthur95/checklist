import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Check } from './check';
import { CheckDataService } from './check-data.service';

@Injectable()
export class ChecksResolver implements Resolve<Observable<Check[]>> {

  constructor(
    private checkDataService: CheckDataService
  ) {
  }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Check[]> {
    return this.checkDataService.allChecks();
  }
}
