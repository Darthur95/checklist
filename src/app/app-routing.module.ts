import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ChecksComponent } from './checks/checks.component';
import { CanActivateChecksGuard } from './can-activate-checks.guard';
import { ChecksResolver } from './checks.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'checks',
    component: ChecksComponent,
    canActivate: [
      CanActivateChecksGuard
    ],
    resolve: {
      checks: ChecksResolver
    }
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    CanActivateChecksGuard,
    ChecksResolver
  ]
})
export class AppRoutingModule { }
