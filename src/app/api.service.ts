import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Check } from './check';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { SessionService } from 'app/session.service';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { map } from 'rxjs/operators';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
    private session: SessionService
  ) {
  }

  public signIn(username: string, password: string) {
    return this.http
      .post(API_URL + '/sign-in', {
        username,
        password
      })
      .catch(this.handleError);
  }

  public allChecks(): Observable<Check[]> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/checks', options)
      .pipe(
        map(response => {
          const checks = <any[]> response;
          return checks.map((check) => new Check(check));
        })
      )
      .catch(this.handleError);
  }

  public createCheck(check: Check): Observable<Check> {
    const options = this.getRequestOptions();
    return this.http
      .post(API_URL + '/checks', check, options)
      .pipe(
        map(response => {
          return new Check(response);
        })
      )
      .catch(this.handleError);
  }

  public checkById(checkId: number): Observable<Check> {
    const options = this.getRequestOptions();
    return this.http
      .get(API_URL + '/checks/' + checkId, options)
      .pipe(
        map(response => {
          return new Check(response);
        })
      )
      .catch(this.handleError);
  }

  public updateCheck(check: Check): Observable<Check> {
    const options = this.getRequestOptions();
    return this.http
      .put(API_URL + '/checks/' + check.id, check, options)
      .pipe(
        map(response => {
          return new Check(response);
        })
      )
      .catch(this.handleError);
  }

  public deleteCheckById(checkId: number): Observable<null> {
    const options = this.getRequestOptions();
    return this.http
      .delete(API_URL + '/checks/' + checkId, options)
      .catch(this.handleError);
  }

  private handleError(error: HttpErrorResponse | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

  private getRequestOptions() {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.session.accessToken
    });
    return { headers };
  }
}
