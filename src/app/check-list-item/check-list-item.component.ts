import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Check } from '../check';

@Component({
  selector: 'app-check-list-item',
  templateUrl: './check-list-item.component.html',
  styleUrls: ['./check-list-item.component.css']
})
export class CheckListItemComponent {

  @Input() check: Check;

  @Output()
  remove: EventEmitter<Check> = new EventEmitter();

  @Output()
  toggleComplete: EventEmitter<Check> = new EventEmitter();

  constructor() {
  }

  toggleCheckComplete(check: Check) {
    this.toggleComplete.emit(check);
  }

  removeCheck(check: Check) {
    this.remove.emit(check);
  }

}
