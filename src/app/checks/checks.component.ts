import { Component, OnInit } from '@angular/core';
import { CheckDataService } from '../check-data.service';
import { Check } from '../check';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { map } from 'rxjs/operators';
import {v4} from 'uuid';

@Component({
  selector: 'app-checks',
  templateUrl: './checks.component.html',
  styleUrls: ['./checks.component.css']
})
export class ChecksComponent implements OnInit {

  checks: Check[] = [];

  constructor(
    private checkDataService: CheckDataService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router
  ) {
  }

  public ngOnInit() {
    this.displayUuid();
    this.route.data
      .pipe(
        map((data) => data['checks'])
      )
      .subscribe(
        (checks) => {
          this.checks = checks;
        }
      );
  }

  onAddCheck(check) {
    this.checkDataService
      .addCheck(check)
      .subscribe(
        (newCheck) => {
          this.checks = this.checks.concat(newCheck);
        }
      );
  }

  onToggleCheckComplete(check) {
    this.checkDataService
      .toggleCheckComplete(check)
      .subscribe(
        (updatedCheck) => {
          check = updatedCheck;
        }
      );
  }

  onRemoveCheck(check) {
    this.checkDataService
      .deleteCheckById(check.id)
      .subscribe(
        (_) => {
          this.checks = this.checks.filter((t) => t.id !== check.id);
        }
      );
  }

  public doSignOut() {
    this.auth.doSignOut();
    this.router.navigate(['/sign-in']);
  }

  displayUuid(){
    console.log('Hello', v4());
  }

}
