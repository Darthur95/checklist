/* tslint:disable:no-unused-variable */

import {TestBed, inject} from '@angular/core/testing';
import {CheckDataService} from './check-data.service';
import { ApiService } from './api.service';
import { ApiMockService } from './api-mock.service';

describe('CheckDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CheckDataService,
        {
          provide: ApiService,
          useClass: ApiMockService
        }
      ]
    });
  });

  it('should ...', inject([CheckDataService], (service: CheckDataService) => {
    expect(service).toBeTruthy();
  }));

});
