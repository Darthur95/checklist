import { Injectable } from '@angular/core';
import { Check } from './check';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class ApiMockService {

  constructor(
  ) {
  }

  public allChecks(): Observable<Check[]> {
    return Observable.of([
      new Check({id: 1, title: 'Read article', complete: false})
    ]);
  }

  public createCheck(check: Check): Observable<Check> {
    return Observable.of(
      new Check({id: 1, title: 'Read article', complete: false})
    );
  }

  public checkById(checkId: number): Observable<Check> {
    return Observable.of(
      new Check({id: 1, title: 'Read article', complete: false})
    );
  }

  public updateCheck(check: Check): Observable<Check> {
    return Observable.of(
      new Check({id: 1, title: 'Read article', complete: false})
    );
  }

  public deleteCheckById(checkId: number): Observable<null> {
    return null;
  }
}
