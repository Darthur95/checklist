import { Component, Output, EventEmitter } from '@angular/core';
import { Check } from '../check';

@Component({
  selector: 'app-check-list-header',
  templateUrl: './check-list-header.component.html',
  styleUrls: ['./check-list-header.component.css']
})
export class CheckListHeaderComponent {

  newCheck: Check = new Check();

  @Output()
  add: EventEmitter<Check> = new EventEmitter();

  constructor() {
  }

  addCheck() {
    this.add.emit(this.newCheck);
    this.newCheck = new Check();
  }

}
