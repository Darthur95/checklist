import {Check} from './check';

describe('Check', () => {
  it('should create an instance', () => {
    expect(new Check()).toBeTruthy();
  });

  it('should accept values in the constructor', () => {
    const check = new Check({
      title: 'hello',
      complete: true
    });
    expect(check.title).toEqual('hello');
    expect(check.complete).toEqual(true);
  });
});
