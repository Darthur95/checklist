import { Injectable } from '@angular/core';
import { Check } from './check';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/delay';

@Injectable()
export class CheckDataService {

  constructor(
    private api: ApiService
  ) {
  }

  // Simulate POST /checks
  addCheck(check: Check): Observable<Check> {
    return this.api.createCheck(check);
  }

  // Simulate DELETE /checks/:id
  deleteCheckById(checkId: number): Observable<Check> {
    return this.api.deleteCheckById(checkId);
  }

  // Simulate PUT /checks/:id
  updateCheck(check: Check): Observable<Check> {
    return this.api.updateCheck(check);
  }

  // Simulate GET /checks
  allChecks(): Observable<Check[]> {
    return this.api.allChecks().delay(3000);
  }

  // Simulate GET /checks/:id
  checkById(checkId: number): Observable<Check> {
    return this.api.checkById(checkId);
  }

  // Toggle complete
  toggleCheckComplete(check: Check) {
    check.complete = !check.complete;
    return this.api.updateCheck(check);
  }

}
