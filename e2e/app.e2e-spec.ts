import { CheckAppPage } from './app.po';

describe('check-app App', () => {
  let page: CheckAppPage;

  beforeEach(() => {
    page = new CheckAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
